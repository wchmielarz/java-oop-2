package com.wchmielarz.app;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class ManagerWhoseHiringAbilityIsLimitedByFixedLimitOfEmployeesCount extends Manager{
    int limitToHire;

    public ManagerWhoseHiringAbilityIsLimitedByFixedLimitOfEmployeesCount(String name, int salary, int limitToHire) {
        super(name,salary);
        this.limitToHire = limitToHire;
    }


    @Override
    protected boolean canHire(int limit){
        return sumOfEmployees < limit;

    }

    @Override
    protected void hireEmployee(Employee employee){
        if (canHire(limitToHire)){
            subordinates.add(employee);
            listOfEmployees.add(employee);
            sumOfSalaries+=employee.salary;
            sumOfEmployees++;
        }

    }

}
