package com.wchmielarz.app;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class Company {
    private static Company ourInstance = new Company();
    private CEO CEO;

    public static Company getInstance() {
        return ourInstance;
    }

    public Company() {}

    protected void hireCEO(CEO CEO){
        this.CEO = CEO;
    }



}
