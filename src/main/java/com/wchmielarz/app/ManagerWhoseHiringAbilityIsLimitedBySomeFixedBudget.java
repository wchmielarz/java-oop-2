package com.wchmielarz.app;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget extends Manager {
    private int limit;

    public ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget(String name, int salary, int limit) {
        super(name, salary);
        this.limit = limit;
    }

    @Override
    protected boolean canHire(int salary){
        return salary < limit;
    }

    @Override
    protected void hireEmployee(Employee employee){
        if (canHire(limit)){
            subordinates.add(employee);
            listOfEmployees.add(employee);
            sumOfSalaries+=employee.salary;
            sumOfEmployees++;
        }
    }
}
