package com.wchmielarz.app;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class CEO extends Manager {

    public CEO (String name, int salary){
        super(name, salary);
    }

    private boolean canHire(){
        return true; //CEO wszystko moze
    }

    @Override
    protected void hireEmployee(Employee employee){
        if (canHire()){
            subordinates.add(employee);
            listOfEmployees.add(employee);
            sumOfSalaries+=employee.salary;
            sumOfEmployees++;
        }
    }

    @Override
    public String toString() {
        return "CEO";
    }
}
