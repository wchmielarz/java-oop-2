package com.wchmielarz.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wchmielarz on 09.04.16.
 */
public abstract class Manager extends Employee {
    List<Employee> listOfEmployees = new ArrayList<Employee>();

    public Manager (String name, int salary){
        super(name,salary);
    }

    protected boolean canHire(int budget){
        return false;
    }

    protected void hireEmployee(Employee employee){
        //do nothing
    }

    @Override
    public String toString() {
        return "Manager";
    }

}
