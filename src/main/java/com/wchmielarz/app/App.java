package com.wchmielarz.app;

/**
 * Created by wchmielarz on 09.04.16.
 */

public class App {

    public static void main(String[] args) {
        Company company = new Company();
        CEO CEO = new CEO("Zbyszek",24000);
        company.hireCEO(CEO);

        Manager manager1 = new ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget("Maciek",12000,7000);
        Manager manager2 = new ManagerWhoseHiringAbilityIsLimitedByFixedLimitOfEmployeesCount("Krzysiek",8000,8);
        Employee employee1 = new Employee("Grzesiek",5000);

        CEO.hireEmployee(manager1);
        CEO.hireEmployee(employee1);
        CEO.hireEmployee(manager2);

        Employee employee2 = new Employee("Wacek",6000);
        Employee employee3 = new Employee("Leszek",5000);
        Employee employee4 = new Employee("Frodo",4000);

        manager1.hireEmployee(employee2);
        manager1.hireEmployee(employee3);
        manager2.hireEmployee(employee4);

        displayHierarchy(CEO,"");
    }


    private static void displayHierarchy(Employee e, String indentation){
        System.out.format("%s%s - %s\n",indentation+="\t",e.name,e.toString());
        for (int i = 0; i < e.subordinates.size();i++) {
            displayHierarchy(e.subordinates.get(i),indentation);
        }
    }

}

