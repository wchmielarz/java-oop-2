package com.wchmielarz.app;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class ManagerTest {

    private int budget = 6000;
    private Manager manager1;
    private Employee employee1;

    @Before
    public void setUp(){
        manager1 = new ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget("Wojtek",12000,budget);
        employee1 = new Employee("Grzegórz",7000);
    }


    @Test
    public void testAbilityToHireEmployees() {
        assertEquals(true,manager1.canHire(employee1.salary));
    }

}
