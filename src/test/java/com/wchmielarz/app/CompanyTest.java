package com.wchmielarz.app;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class CompanyTest {

    private Manager manager1;
    private Employee employee1;
    private CEO CEO;

    @Before
    public void setUp(){
        manager1 = new ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget("Wojtek",12000,0);
        employee1 = new Employee("Krisztianu",7000);
        CEO = new CEO("Meszi",0);
    }

    @Test
    public void toStringShouldReturnProperString() {
        assertEquals("CEO",CEO.toString());
        assertEquals("Manager",manager1.toString());
        assertEquals("Employee",employee1.toString());
    }

}
