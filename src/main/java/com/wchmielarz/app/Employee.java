package com.wchmielarz.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wchmielarz on 09.04.16.
 */
public class Employee {

    protected String name;
    protected int salary;
    protected boolean isSatisfied;
    protected int sumOfSalaries;
    protected int sumOfEmployees;
    protected List<Employee> subordinates = new ArrayList<Employee>();

    public Employee (String name, int salary) {
        this.name = name;
        this.salary = salary;
        this.isSatisfied = salary > 10000;
    }


    protected void sayName(String name) {
        System.out.format("My name is, what? My name is, who? My name is ciki-ciki %s",name);
    }

    protected void saySalary(int salary) {
        System.out.format("My salary is %d",salary);
    }

    protected void saySatisfaction() {
        System.out.format(isSatisfied ? "I'm satisfied" : "I'm not satisfied");
    }

    @Override
    public String toString() {
        return "Employee";
    }

}
