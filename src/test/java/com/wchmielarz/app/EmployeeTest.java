package com.wchmielarz.app;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


/**
 * Created by wchmielarz on 09.04.16.
 */
public class EmployeeTest {

    private Employee employee1;
    private Employee employee2;
    private Employee employee3;
    private Employee employee4;
    private Manager manager1;
    private Manager manager2;
    private int limit = 6;


    @Before
    public void setUp(){

        employee1 = new Employee("Ala",6000);
        employee2 = new Employee("Kala",6000);
        employee3 = new Employee("Wola",6000);
        employee4 = new Employee("Sola",6000);

        manager1 = new ManagerWhoseHiringAbilityIsLimitedByFixedLimitOfEmployeesCount("Ola",12000,limit);
        manager1.hireEmployee(employee1);
        manager1.hireEmployee(employee2);
        manager1.hireEmployee(employee3);


        manager2 = new ManagerWhoseHiringAbilityIsLimitedBySomeFixedBudget("Pola",4000,2000);


    }

    @Test
    public void testSatisfaction(){
        assertEquals(true, manager1.salary > 10000);
        assertEquals(false, employee1.salary > 10000);
    }

    @Test
    public void testProperties(){
        assertEquals("Ala",employee1.name);
        assertEquals(12000, manager1.salary);
    }

    @Test
    public void checkIfManagerHiresProperNumberOfEmployees(){
        assertEquals(true, manager1.subordinates.size() < limit);
    }

    @Test
    public void checkIfManagerIsNotHiringEmployeeWithTooHighSalary(){
        assertFalse(manager2.canHire(employee4.salary));
    }




}
